<?php 
include "..\RangoInforme.php";
	class rangoInformeTest  extends \PHPUnit_Framework_TestCase
	{
		public function testZeroAndZeroReturnsFalse()
		{
			$rangInf=new rangoInforme();
			$this->assertEquals(false,$rangInf->recibirRango(0,0));
		}

		public function testRangeUnder2000ReturnsFalse()
		{
			$rangInf=new rangoInforme();
			$beginRang= "1991-11-11";
			$endRang= "1992-12-12";
			$this->assertEquals(false,$rangInf->recibirRango($beginRang,$endRang));
		}

		public function testEndingRangeBiggerThanTodayReturnsTrue(){
			$rangInf=new rangoInforme();
			$beginRang= "2017-11-30";
			$endRang= "2018-12-08";
			$this->assertEquals(true,$rangInf->recibirRango($beginRang,$endRang));
		}
		public function testBeginYearBiggerThanEndingYearReturnsFalse(){
			$rangInf=new rangoInforme();
			$beginRang= "2003-02-01";
			$endRang= "2001-02-09";
			$this->assertEquals(false,$rangInf->recibirRango($beginRang,$endRang));
		}

		public function testBeginMonthBiggerThanEndingMonthReturnsFalse(){
			$rangInf=new rangoInforme();
			$beginRang= "2004-03-11";
			$endRang= "2004-02-10";
			$this->assertEquals(false,$rangInf->recibirRango($beginRang,$endRang));
		}
		public function testBeginDayBiggerThanEndingDayReturnsFalse(){
			$rangInf=new rangoInforme();
			$beginRang= "2006-09-18";
			$endRang= "2006-09-16";
			$this->assertEquals(false,$rangInf->recibirRango($beginRang,$endRang));
		}

		public function testShortAndCorrectRangeDistanceReturnsTrue(){
			$rangInf=new rangoInforme();
			$beginRang= "2017-6-26";
			$endRang= "2017-7-27";
			$this->assertEquals(true,$rangInf->recibirRango($beginRang,$endRang));			
		}

		public function testSameDayRangeReturnsFalse(){
			$rangInf=new rangoInforme();
			$beginRang= "2017-12-05";
			$endRang= "2017-12-05";
			$this->assertEquals(false,$rangInf->recibirRango($beginRang,$endRang));	
		}

		public function testStringsReturnsFalse(){
			$rangInf=new rangoInforme();
			$beginRang= "cccc-aa-bb";
			$endRang= "aaaa-cc-bb";
			$this->assertEquals(false,$rangInf->recibirRango($beginRang,$endRang));	
		}

		public function testWrongYearValueReturnsFalse(){
			$rangInf=new rangoInforme();
			$beginRang= "25-02-25";
			$endRang= "26-06-12";
			$this->assertEquals(false,$rangInf->recibirRango($beginRang,$endRang));	
		}

		public function testWrongMonthValueReturnsFalse(){
			$rangInf=new rangoInforme();
			$beginRang= "2000-13-20";
			$endRang= "2000-13-30";
			$this->assertEquals(false,$rangInf->recibirRango($beginRang,$endRang));	
		}

		public function testWrongDayValueReturnsFalse(){
			$rangInf=new rangoInforme();
			$beginRang= "2000-12-34";
			$endRang= "2002-01-32";
			$this->assertEquals(false,$rangInf->recibirRango($beginRang,$endRang));	
		}

		public function testNotNumbersValuesReturnsFalse(){
			$rangInf=new rangoInforme();
			$beginRang= "das-%%-24&7";
			$endRang= "0121-$$-23";
			$this->assertEquals(false,$rangInf->recibirRango($beginRang,$endRang));	
		}
	}
?>