<?php  

	class RangoInforme
	{
		public function recibirRango($inicio, $final){
			# $inicio y $final deben de tener el formato año/mes/día para que funcione esta función.
			$arrayInicio = explode('-', $inicio);
			$arrayFinal = explode('-', $final);
			if(count($arrayInicio)!=3 || count($arrayFinal)!=3){
				#Valor ingresado no válido
				return false;
			}else if($arrayFinal[0]<2000 || $arrayInicio[0]<2000){
				#Alguna de las fechas ingresadas están fuera del rango aceptado.
				return false;
			}else if($arrayFinal[1]>12 || $arrayInicio[1]>12 || $arrayFinal[1]<1 || $arrayInicio[1]<1){
				#Mes inválido.
				return false;
			} else 	if($arrayFinal[2]>31 || $arrayInicio[2]>31 || $arrayFinal[2]<1 || $arrayInicio[2]<1){
				#Dia inválido.
				return false;
			}else if($arrayFinal[0]< $arrayInicio[0]){
				# Año de fin de rango menor a año de fin de rango
				return false;
			}else if($arrayFinal[0]== $arrayInicio[0] && $arrayFinal[1]< $arrayInicio[1]){
				# Mismo año de rango, mes de inicio rango mayor a mes final de rango.
				return false;
			}else if($arrayFinal[0]== $arrayInicio[0] && $arrayFinal[1]==$arrayInicio[1] && $arrayFinal[2]<$arrayInicio[2]){
				# Mes y año iguales, día fin de rango menor a dia inicio de rango.
				return false;
			}else if($arrayFinal[0]== $arrayInicio[0] && $arrayFinal[1]==$arrayInicio[1] && $arrayFinal[2]==$arrayInicio[2]){
			# Fecha de inicio y final del rango son iguales.
				return false;
			}else{
				return true;
			}
		}	
	}
?>