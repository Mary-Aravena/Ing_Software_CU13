<h3>4 de diciembre del 2017:</h3>
<ul>
	<li>
		Creaci�n de Proyecto para documentaci�n de pruebas unitarias PHPUnit aplicando TDD vac�o para el caso de uso <b>CU13: Exportaci�n De Archivo Excel</b>.
	</li>
</ul>

<br><h3>5 de diciembre del 2017:</h3>
<ul>	
	<li>
		Ingreso de la primera secci�n de pruebas unitarias PHPUnit aplicando TDD:<br> Prueba del rango ingresado por el usuario al sistema y sus correspondientes particiones de equivalencia.
	</li>
</ul>

<br><h3>8 de diciembre del 2017:</h3>
<ul>	
	<li>
		Inserci�n de nuevas pruebas unitarias en la primera secci�n del caso uso, seg�n la partici�n de equivalencia:<br> 
		<b>P6-Formatos de fechas alterado<b>
	</li>
	<li>
		Implementaci�n de la segunda secci�n de pruebas unitarias PHPUnit aplicando TDD: <br> Prueba de las Querys utilizadas para la obtenci�n de informaci�n directa de las bases de datos a visualizarse en el archivo excel.
	</li>
	
</ul>

<br><h3>9 de diciembre del 2017:</h3>
<ul>	
	<li>
		Inserci�n de primera versi�n de c�digo para la exportaci�n de archivos excel:<br> 
		<b> Sin utilizar PHPExcel<b>
	</li>
	<li>
		Inserci�n de segunda versi�n de c�digo, correspondiente al controlador del caso de uso </b>CU13: Exportaci�n De Archivo Excel</b> para la exportaci�n de archivos excel:<br> 
		<b> Utilizando PHPExcel<b>
	</li>
	
</ul>

<br><h3>10 de diciembre del 2017:</h3>
<ul>	
	<li>
		Inserci�n de la cuarta secci�n de pruebas:<br> 
		S�lo se realizaron los cambios en el m�todo <b>"generar_excel($inicio, $final)"<b> para registrar los cambios en el archivo excel_modelo.
	</li>
</ul>