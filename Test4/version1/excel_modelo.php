<?php
	class excel_modelo extends CI_Model{
		
		public function __construct(){	
	        $this->load->database();
	    }
		public function obtenerDatosProductosParaExcel()
		{
			$query = $this->db->query("SELECT P.id_producto, P.nombre_producto, 
			P.genero_producto, P.autor_producto, P.precio_producto, P.stock_producto 
			FROM producto P");
			return $query->result_array();
	    }
		
		public function obtenerDatosClientesParaExcel()
		{
			$query = $this->db->query("SELECT U.rut_usuario, U.nombre_usuario, U.telefono_usuario, 
			U.comuna_usuario, U.ciudad_usuario FROM usuario U WHERE U.tipo_usuario=1");
			return $query->result_array();
		}
		
		public function obtenerDatosRankingProductos($inicio, $final){
			/*
			$query = $this->db->query("");
			return $query->result_array();
			*/
		}
		public function obtenerDatosAnalisisClientes($inicio, $final){
			/*
			$query = $this->db->query("");
			return $query->result_array();
			*/
		}
	}
?>