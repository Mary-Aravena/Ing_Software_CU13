Hoja de c�lculo con la informaci�n de los clientes cuyas ventas se encuentren registradas entre las fechas ingresadas.

Informaci�n esperada de la base de datos:
[Tabla]
-Rut_usuario
-Nombre_usuario
-Telefono_usuario
-comuna_usuario
-ciudad_usuario
-Ventas_pagadas
-ventas_pendientes
-ventas_no_paga
-ventas_total

Duda: 
�C�mo determinar una venta no paga si es que el sistema no registra el no pago de pedidos?
--Posibles soluciones:
1. Fecha_pago= Fecha_solicitud pedido
2. Fecha_pago= permitir el ingreso en la secci�n de venta.

�Se deber� de considerar la informaci�n de los pedidos?
--Posibles soluciones:
1. No.
2. Si, y aquello podr�a servir para determinar el no pago de una venta asumiendo el plazo de 40 d�as.
3. Si, y para ello se requiere una nueva hoja de c�lculo para el an�lisis de los pedidos generales.