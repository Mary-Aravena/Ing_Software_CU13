<?php 
	class excel_controlador extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->model('excel_modelo');
			$this->load->helper('url_helper');
		}
		
		public function indiceExportarExcel(){
			$this->load->view('plantillas/header');
			$this->load->view('excel/index');
			$this->load->view('plantillas/footer');
		}
		
		public function validarFechas(){
			$inicio = $_POST['fechaInicioRango'];
			$final = $_POST['fechaFinRango'];
			# $inicio y $final deben de tener el formato año/mes/día para que funcione esta función.
			$arrayInicio = explode('-', $inicio);
			$arrayFinal = explode('-', $final);
			$mensaje = '';
			if(count($arrayInicio)!=3 || count($arrayFinal)!=3){
				#Valor ingresado no válido
				$mensaje= 'Error: El formato ingresado no corresponde al formato aceptado por el sistema. Formato aceptado: aaaa-mm-dd';
			}else if($arrayFinal[0]<2000 || $arrayInicio[0]<2000){
				#Alguna de las fechas ingresadas están fuera del rango aceptado.
				$mensaje= 'Error: Alguna de las fechas ingresadas están fuera del rango aceptado para la generación del informe';
			}else if($arrayFinal[1]>12 || $arrayInicio[1]>12 || $arrayFinal[1]<1 || $arrayInicio[1]<1){
				#Mes inválido.
				$mensaje= 'Error: Alguno de los meses ingresados posee algún error de rango. Formato aceptado: aaaa-mm-dd';
			} else if($arrayFinal[2]>31 || $arrayInicio[2]>31 || $arrayFinal[2]<1 || $arrayInicio[2]<1){
				#Dia inválido.
				$mensaje= 'Error: Alguno de los días ingresados posee algún error de rango. Formato aceptado: aaaa-mm-dd';
			}else if($arrayFinal[0]< $arrayInicio[0]){
				# Año de fin de rango menor a año de fin de rango
				$mensaje= 'Error: El año del fin del rango es menor al año del inicio de rango.';
			}else if($arrayFinal[0]== $arrayInicio[0] && $arrayFinal[1]< $arrayInicio[1]){
				# Mismo año de rango, mes de inicio rango mayor a mes final de rango.
				$mensaje= 'Error: El mes del fin del rango es menor al mes de inicio de rango en el mismo año.';
			} else if($arrayFinal[0]== $arrayInicio[0] && $arrayFinal[1]==$arrayInicio[1] && $arrayFinal[2]<$arrayInicio[2]){
				# Mes y año iguales, día fin de rango menor a dia inicio de rango.
				$mensaje= 'Error: El día del fin del rango es menor al día de inicio de rango en el mismo año y mes';
			}else if($arrayFinal[0]== $arrayInicio[0] && $arrayFinal[1]==$arrayInicio[1] && $arrayFinal[2]==$arrayInicio[2]){
				# Fecha de inicio y final del rango son iguales.
				$mensaje= 'Error: Las fechas del rango deben ser diferentes entre sí.';
			}
			
			if ($mensaje!=''){ 
				#Si el mensaje es distinto de vacio, volver a la vista enviando el mensaje de error
				$data['mensaje'] = $mensaje; #En la vista se accede a 'mensaje' no a 'data'
				$this->load->view('plantillas/header');
				$this->load->view('excel/index', $data);
				$this->load->view('plantillas/footer');
				
			} else { 
				#Si el mensaje es vacio, se genera el Excel
				$this->generar_excel();
				
			}		
		}
		
		public function generar_excel(){
		$datosProductos = $this->excel_modelo->obtenerDatosProductosParaExcel();
		$datosClientes = $this->excel_modelo->obtenerDatosClientesParaExcel();
		if(count($datosProductos) > 0){
			//Cargamos la librería de excel.
			$this->load->library('excel');
		//PRIMERA PAGINA - DATOS PRODUCTOS
				$this->excel->setActiveSheetIndex(0);
				$this->excel->getActiveSheet()->setTitle('Info Productos');
			  //Aliniando columnas
				$this->excel->getDefaultStyle()->getAlignment()->setWrapText(true);
				$this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);			
				$this->excel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
			  //Le aplicamos ancho las columnas.
				$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
				$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
				$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
				$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
				$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
			  //Titulo de la página
				$this->excel->getActiveSheet()->setCellValue("B1", 'PRODUCTOS EXISTENTES EN EL SISTEMA');
				$this->excel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true);
				$this->excel->getActiveSheet()->getStyle("B1")->getFont()->setSize(13);
				$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
				$this->excel->setActiveSheetIndex(0)->mergeCells('B1:G1');
			  //Contador de filas
				$contador = 2;
				//Le aplicamos negrita a los títulos de la cabecera.
				$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
				$this->excel->getActiveSheet()->getStyle("C{$contador}")->getFont()->setBold(true);
				$this->excel->getActiveSheet()->getStyle("D{$contador}")->getFont()->setBold(true);
				$this->excel->getActiveSheet()->getStyle("E{$contador}")->getFont()->setBold(true);
				$this->excel->getActiveSheet()->getStyle("F{$contador}")->getFont()->setBold(true);
				$this->excel->getActiveSheet()->getStyle("G{$contador}")->getFont()->setBold(true);
				//Definimos los títulos de la cabecera.
				$this->excel->getActiveSheet()->setCellValue("B{$contador}", 'ID Producto');
				$this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Nombre Producto');
				$this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Genero Producto');
				$this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Autor Producto');
				$this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Precio Producto');
				$this->excel->getActiveSheet()->setCellValue("G{$contador}", 'Stock Producto');
				//Definimos la data del cuerpo.        
				foreach($datosProductos as $l){
				   //Incrementamos una fila más, para ir a la siguiente.
				   $contador++;
				   //Informacion de las filas de la consulta.
				   $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l['id_producto']);
				   $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l['nombre_producto']);
				   $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l['genero_producto']);
				   $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l['autor_producto']);
				   $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l['precio_producto']);
				   $this->excel->getActiveSheet()->setCellValue("G{$contador}", $l['stock_producto']);
				}
		//SEGUNDA PAGINA - DATOS CLIENTES
				$this->excel->createSheet();
				$this->excel->setActiveSheetIndex(1);
				$this->excel->getActiveSheet()->setTitle('Info Clientes');
			  //Aliniando columnas
				$this->excel->getDefaultStyle()->getAlignment()->setWrapText(true);
				$this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);			
				$this->excel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
			  //Le aplicamos ancho las columnas.
				$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
				$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
				$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
				$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
				$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
			  //Titulo de la página
				$this->excel->getActiveSheet()->setCellValue("B1", 'CLIENTES REGISTRADOS EN EL SISTEMA');
				$this->excel->getActiveSheet()->getStyle("B1")->getFont()->setBold(true);
				$this->excel->getActiveSheet()->getStyle("B1")->getFont()->setSize(13);
				$this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
				$this->excel->setActiveSheetIndex(1)->mergeCells('B1:F1');
			  //Contador de filas
				$contador = 2;	
				//Le aplicamos negrita a los títulos de la cabecera.
				$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
				$this->excel->getActiveSheet()->getStyle("C{$contador}")->getFont()->setBold(true);
				$this->excel->getActiveSheet()->getStyle("D{$contador}")->getFont()->setBold(true);
				$this->excel->getActiveSheet()->getStyle("E{$contador}")->getFont()->setBold(true);
				$this->excel->getActiveSheet()->getStyle("F{$contador}")->getFont()->setBold(true);
				//Definimos los títulos de la cabecera.
				$this->excel->getActiveSheet()->setCellValue("B{$contador}", 'RUT Cliente');
				$this->excel->getActiveSheet()->setCellValue("C{$contador}", 'Nombre Cliente');
				$this->excel->getActiveSheet()->setCellValue("D{$contador}", 'Telefono Cliente');
				$this->excel->getActiveSheet()->setCellValue("E{$contador}", 'Comuna de Residencia Cliente');
				$this->excel->getActiveSheet()->setCellValue("F{$contador}", 'Ciudad de Residencia Cliente');
				foreach($datosClientes as $l){
				   //Incrementamos una fila más, para ir a la siguiente.
				   $contador++;
				   //Informacion de las filas de la consulta.
				   $this->excel->getActiveSheet()->setCellValue("B{$contador}", $l['rut_usuario']);
				   $this->excel->getActiveSheet()->setCellValue("C{$contador}", $l['nombre_usuario']);
				   $this->excel->getActiveSheet()->setCellValue("D{$contador}", $l['telefono_usuario']);
				   $this->excel->getActiveSheet()->setCellValue("E{$contador}", $l['comuna_usuario']);
				   $this->excel->getActiveSheet()->setCellValue("F{$contador}", $l['ciudad_usuario']);
				}
		//TERCERA PAGINA - RANKING PRODUCTOS
				#TODO
		//CUARTA  PÁGINA - CLIENTES Y VENTAS
				#TODO
			//Le ponemos un nombre al archivo que se va a generar.
			$archivo = "informe_excel.xls";
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$archivo.'"');
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			//Hacemos una salida al navegador con el archivo Excel.
			$objWriter->save('php://output');
		 }else{
			echo 'No se han encontrado llamadas';
			exit;        
		 }
  }
	}
?>