<?php 
	class excel_controlador extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->model('excel_modelo');
			$this->load->helper('url_helper');
		}
		
		public function indiceExportarExcel(){
			$this->load->view('plantillas/header');
			$this->load->view('excel/index');
			$this->load->view('plantillas/footer');
		}
		
		public function validarFechas(){
			$inicio = $_POST['fechaInicioRango'];
			$final = $_POST['fechaFinRango'];
			# $inicio y $final deben de tener el formato año/mes/día para que funcione esta función.
			$arrayInicio = explode('-', $inicio);
			$arrayFinal = explode('-', $final);
			$mensaje = '';
			if(count($arrayInicio)!=3 || count($arrayFinal)!=3){
				#Valor ingresado no válido
				$mensaje= 'Error: El formato ingresado no corresponde al formato aceptado por el sistema. Formato aceptado: aaaa-mm-dd';
			}else if($arrayFinal[0]<2000 || $arrayInicio[0]<2000){
				#Alguna de las fechas ingresadas están fuera del rango aceptado.
				$mensaje= 'Error: Alguna de las fechas ingresadas están fuera del rango aceptado para la generación del informe';
			}else if($arrayFinal[1]>12 || $arrayInicio[1]>12 || $arrayFinal[1]<1 || $arrayInicio[1]<1){
				#Mes inválido.
				$mensaje= 'Error: Alguno de los meses ingresados posee algún error de rango. Formato aceptado: aaaa-mm-dd';
			} else if($arrayFinal[2]>31 || $arrayInicio[2]>31 || $arrayFinal[2]<1 || $arrayInicio[2]<1){
				#Dia inválido.
				$mensaje= 'Error: Alguno de los días ingresados posee algún error de rango. Formato aceptado: aaaa-mm-dd';
			}else if($arrayFinal[0]< $arrayInicio[0]){
				# Año de fin de rango menor a año de fin de rango
				$mensaje= 'Error: El año del fin del rango es menor al año del inicio de rango.';
			}else if($arrayFinal[0]== $arrayInicio[0] && $arrayFinal[1]< $arrayInicio[1]){
				# Mismo año de rango, mes de inicio rango mayor a mes final de rango.
				$mensaje= 'Error: El mes del fin del rango es menor al mes de inicio de rango en el mismo año.';
			} else if($arrayFinal[0]== $arrayInicio[0] && $arrayFinal[1]==$arrayInicio[1] && $arrayFinal[2]<$arrayInicio[2]){
				# Mes y año iguales, día fin de rango menor a dia inicio de rango.
				$mensaje= 'Error: El día del fin del rango es menor al día de inicio de rango en el mismo año y mes';
			}else if($arrayFinal[0]== $arrayInicio[0] && $arrayFinal[1]==$arrayInicio[1] && $arrayFinal[2]==$arrayInicio[2]){
				# Fecha de inicio y final del rango son iguales.
				$mensaje= 'Error: Las fechas del rango deben ser diferentes entre sí.';
			}
			
			if ($mensaje!=''){ 
				#Si el mensaje es distinto de vacio, volver a la vista enviando el mensaje de error
				$data['mensaje'] = $mensaje; #En la vista se accede a 'mensaje' no a 'data'
				$this->load->view('plantillas/header');
				$this->load->view('excel/index', $data);
				$this->load->view('plantillas/footer');
				
			} else { 
				#Si el mensaje es vacio, se genera el Excel
				#Generación primera hoja de cálculo.
					$allData = $this->excel_modelo->obtenerDatosProductosParaExcel();
					$dataToExports = [];
					foreach ($allData as $data) {
						$arrangeData['Id Producto'] = $data['id_producto'];
						$arrangeData['Nombre Producto'] = $data['nombre_producto']; 
						$arrangeData['Genero Musical'] = $data['genero_producto'];
						$arrangeData['Autor'] = $data['autor_producto']; 
						$arrangeData['Precio Unitario'] = $data['precio_producto'];
						$arrangeData['Stock Existente'] = $data['stock_producto'];
						
						$dataToExports[] = $arrangeData;
					}
					// set header
					$filename = "InformeExcel";
					$nameHC = "InfoProductos";
								header( "Content-type: application/vnd.ms-excel; charset=UTF-8");
								header("Content-Disposition: attachment; filename=\"$filename\"");
							
					$this->exportExcelData($dataToExports, "Productos Registrados en el Sistema");
				#Generación segunda hoja de cálculo.
				
				
			}		
		}
		
		public function exportExcelData($records, $titulo)
		{
			$heading = "InfoProductos";
			if (!empty($records)){
				echo $titulo;
				echo "\n";
				foreach ($records as $row) {
					if (!$heading) {
						// display field/column names as a first row
						echo implode("\t", array_keys($row)) . "\n";
						$heading = true;
					}
					echo implode("\t", ($row)) . "\n";
				}
			}
		}
	}
?>