<?php 
include "..\AnalisisQuery.php";
	class analisisQueryTest  extends \PHPUnit_Framework_TestCase
	{
		public function testNothingReturnsFalse()
		{
			$anaQ=new AnalisisQuery();
			$myQuery = '';
			$this->assertEquals(false,$anaQ->analizarQueryProducto($myQuery));
		}

		public function testCorrectQueryReturnsTrue()
		{
			$anaQ=new AnalisisQuery();
$myQuery = 'SELECT P.id_producto, P.nombre_producto, P.genero_producto, P.autor_producto, P.precio_producto, P.stock_producto FROM productos P';
			$this->assertEquals(true,$anaQ->analizarQueryProducto($myQuery));
		}

		public function testWrongFromReturnsFalse()
		{
			$anaQ=new AnalisisQuery();
			$myQuery = 'SELECT P.id_producto, P.nombre_producto, P.genero_producto, P.autor_producto, P.precio_producto, P.stock_producto FROM usuarios P';
			$this->assertEquals(false,$anaQ->analizarQueryProducto($myQuery));
		}
	}
?>